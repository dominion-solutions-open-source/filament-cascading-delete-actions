<?php

namespace DominionSolutions\FilamentCascadingActions\Actions;

use Filament\Actions\Concerns\CanCustomizeProcess;
use Filament\Actions\DeleteAction;
use Illuminate\Database\Eloquent\Model;

class DeleteCascadingAction extends DeleteAction
{
    use CanCustomizeProcess;

    public function relationships(string ...$relationships): static
    {
        $this->before(fn (Model $record) => collect($relationships)->each(fn (string $relationship) => $record->{$relationship}()->delete()));

        return $this;
    }
}

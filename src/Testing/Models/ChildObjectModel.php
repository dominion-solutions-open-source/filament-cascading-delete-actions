<?php

namespace DominionSolutions\FilamentCascadingActions\Testing\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildObjectModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function parentObjectModel()
    {
        return $this->belongsTo(ParentObjectModel::class);
    }
}

<?php

namespace DominionSolutions\FilamentCascadingActions\Testing\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParentObjectModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function childObjectModels()
    {
        return $this->hasMany(ChildObjectModel::class);
    }
}

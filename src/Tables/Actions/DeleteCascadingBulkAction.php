<?php

namespace DominionSolutions\FilamentCascadingActions\Tables\Actions;

use Filament\Actions\Concerns\CanCustomizeProcess;
use Filament\Tables\Actions\DeleteBulkAction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DeleteCascadingBulkAction extends DeleteBulkAction
{
    use CanCustomizeProcess;

    public function relationships(string ...$relationships): static
    {
        $this->before(fn (Collection $records) => $records->each(
            fn (Model $record) => collect($relationships)->each(fn (string $relationship) => $record->{$relationship}()->delete())
        ));

        return $this;
    }
}

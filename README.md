# A filament package that implements cascading and bulk actions.

[![Latest Version on Packagist](https://img.shields.io/packagist/v/dominion-solutions/filament-cascading-actions.svg?style=flat-square)](https://packagist.org/packages/dominion-solutions/filament-cascading-actions)
[![Total Downloads](https://img.shields.io/packagist/dt/dominion-solutions/filament-cascading-actions.svg?style=flat-square)](https://packagist.org/packages/dominion-solutions/filament-cascading-actions)

[Skip To Installation Instructions](#installation)

## What does this package do?
This package brings cascading database operations to Filament, utilizing Actions.  There are times that you may not want a `CASCADE` option on your foreign relationsips in the database, as well as times where you may not own the data schema outright.  This package fills that gap.  The cascading actions can be included as Bulk Actions, on a listing table, as well as a singular action, on either an edit page or on a record on a table.  It offers **no** UI components itself, but rather uses the UI elements for the action that it is wrapping (`DeleteAction` for example).

### Why Would you want to do a Cascading Action on a relationship?
Consider the following schema:
![Tax Rate Schema](doc/images/schema_relationships.svg)

Tax Rate Amount Model
```php
// ...
class TaxRateAmount extends BaseModel
{
    //...

    /**
     * Return the tax rate relation.
     */
    public function taxRate(): BelongsTo
    {
        return $this->belongsTo(TaxRate::class);
    }

    /**
     * Return the tax class relation.
     */
    public function taxClass(): BelongsTo
    {
        return $this->belongsTo(TaxClass::class);
    }
}
```

Tax Rate Model:
```php
// ...
class TaxRate extends BaseModel
{
    //...
    /**
     * Return the tax zone relation.
     */
    public function taxZone(): BelongsTo
    {
        return $this->belongsTo(TaxZone::class);
    }

    /**
     * Return the tax rate amounts relation.
     */
    public function taxRateAmounts(): HasMany
    {
        return $this->hasMany(TaxRateAmount::class);
    }
}

```
In this case, deleting the `TaxRate` that is associated with a `TaxRateAmount` will cause a foreign key constraint error in the database.  In this case, the `DeleteCascadingAction` and `DeleteCascadingBulkAction` Actions would be the appropriate actions to use, using `->relationship('taxRateAmounts')` on each.

## Currently Supported Cascading Actions
Currently the following cascading actions are supported:
* Delete: ```DominionSolutions\FilamentCascadingActions\Actiouns\DeleteCascadingAction```
* BulkDelete: ```DominionSolutions\FilamentCascadingActions\Tables\Actions\DeleteCascadingBulkAction```

## The Relationships Method
The `relationships()` method is exposed on each of the actions.  Right now the default action is to not traverse the relathionships at all, effectively acting like a the regular Filament Action that it wraps.  It accepts an array of strings with the names of the relationships that you would like to modify along with the parent record.

## That's great, but what is a cascading database action?
In the context of databases, "cascading" refers to the way changes in one part of the database can affect other related parts. Specifically, cascading operations typically involve actions like inserting, updating, or deleting data.

### 1. Cascading Inserts:
- Suppose you have two tables, let's call them Table A and Table B, and there's a relationship between them.
- A cascading insert means that when you add a new record to Table A, related records in Table B are automatically created. This ensures that the data in both tables stay connected.

### 2. Cascading Updates:
- Let's say you change the value of a particular field in a record of Table A. With cascading updates, the corresponding records in Table B that are linked to the updated record in Table A also get automatically updated to reflect the change.

### 3. Cascading Deletes:
- Similarly, when you delete a record in Table A, cascading deletes ensure that associated records in Table B are also deleted. This helps maintain data integrity, preventing orphaned records in related tables.

## Installation

You can install the package via composer:

```bash
composer require dominion-solutions/filament-cascading-actions
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="filament-cascading-actions-config"
```

This is the contents of the published config file:

```php
return [
    return [
    'enable-cascading-actions' => true,
    'prompt-before-cascading' => true,
    'bulk-delete-action-name' => 'Delete',
];
```

## Usage
Within the Header Actions for a Filament Page:
```php
protected function getHeaderActions(): array
{
    return [
        \DominionSolutions\FilamentCascadingActions\Actions\DeleteCascadingAction::make()
            ->relationships('taxRateAmounts'),
    ];
}
```

Using a Bulk Action on a Table:
```php
public static function table(Table $table): Table
{
    return $table
        ->columns([
            // ...
        ])
        ->filters([
            // ...
        ])
        ->actions([
            // ...
        ])
        ->bulkActions([
            Tables\Actions\BulkActionGroup::make([
                DeleteCascadingBulkAction::make()->relationships('taxRateAmounts'),
            ]),
    ]);
}
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Mark J. Horninger](https://github.com/dominion-solutions-open-source)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

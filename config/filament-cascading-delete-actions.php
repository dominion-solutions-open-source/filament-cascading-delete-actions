<?php

// config for DominionSolutions/FilamentCascadingActions
return [
    'enable-cascading-actions' => true,
    'prompt-before-cascading' => true,
    'bulk-delete-action-name' => 'Delete',
];
